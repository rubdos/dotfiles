fish_vi_key_bindings

set PATH $HOME/.local/bin $HOME/bin $HOME/.cargo/bin $HOME/node_modules/rollup/dist/bin/ $PATH

fish_ssh_agent

set EDITOR vim
set VISUAL vim

set ANSIBLE_NOCOWS 1

alias vim=nvim
alias vi=nvim
