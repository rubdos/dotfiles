" setlocal conceallevel=1
let g:vim_markdown_fenced_languages = ['c++=cpp', 'viml=vim', 'bash=sh', 'ini=dosini', 'rust=rs']

set textwidth=79
