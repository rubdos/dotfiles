set spelllang=en_gb spell

setlocal tabstop=2
setlocal softtabstop=2
setlocal shiftwidth=2

augroup TexConfig
  au!
  " autocmd Filetype tex map <C-s-n> <ESC>:VimtexTocToggle<CR><C-w><Left>
  autocmd Filetype tex,ly map <F4> <ESC>:!make<CR>
  let g:tex_flavor = 'latex'
  "autocmd BufWritePost tex :LanguageToolCheck
  autocmd Filetype tex :call vimtex#init()
  autocmd Filetype tex setlocal ts=2 sts=2 sw=2
  autocmd FileType tex let b:vimtex_main = 'main.tex'
augroup END

setlocal conceallevel=2
let g:tex_conceal = 'abdgm'
let g:vimtex_fold_enabled=1
" let g:fillchars=vert:|,fold:\
" let g:vimtex_fold_types = g:vimtext_fold_types_defaults

map <leader>v <plug>(vimtex-view)
map <leader>ll <plug>(vimtex-view)

let g:vimtex_complete_enabled=1
let g:vimtex_complete_recursive_bib = 1
let g:vimtex_enabled = 1

call deoplete#custom#var('omni', 'input_patterns', {
\ 'tex': g:vimtex#re#deoplete,
\ })

let g:vimtex_compiler_latexmk = {
    \ 'backend' : 'jobs',
    \ 'background' : 1,
    \ 'build_dir' : '',
    \ 'callback' : 1,
    \ 'continuous' : 1,
    \ 'executable' : 'latexmk',
    \ 'options' : [
    \   '-pdf',
    \   '--shell-escape',
    \   '-verbose',
    \   '-file-line-error',
    \   '-synctex=1',
    \   '-interaction=nonstopmode',
    \ ],
    \}
