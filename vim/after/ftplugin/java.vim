map <F4> <ESC>:Mvn exec:java<CR>
map <C-F4> <ESC>:Mvn test<CR>
map <F9> <ESC>:JavaImport<CR>
map <F6> <ESC>:JavaCorrect<CR>

nmap <leader>i <ESC>:JavaImport<CR>
nnoremap <silent> <buffer> <cr> :JavaSearchContext<cr>

" au BufWritePost *.java,*.at silent! !ctags -R &

let g:EclimCompletionMethod = 'omnifunc'
