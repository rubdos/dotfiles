map sav sacvec<cr>

let g:tex_flavor = 'latex'

" Format table
map <leader>ft Vie:Tabularize /&<cr>Vie:Tabularize /\\\\ <cr>
