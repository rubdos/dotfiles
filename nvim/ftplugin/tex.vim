let g:vimtex_view_method = 'zathura'
let g:latex_view_general_viewer = 'zathura'

let g:sandwich#recipes += [{'buns': ['\vec{', '}'], 'input': ['v']}]

" Below is now handled 'intelligently' in autocomplete plugin
" map <buffer> K <Plug>(vimtex-doc-package)

set spelllang=en_gb spell

setlocal tabstop=2 softtabstop=2 shiftwidth=2

setlocal conceallevel=2
let g:tex_conceal = 'abd'
let g:vimtex_fold_enabled=1

map <leader>v <plug>(vimtex-view)
