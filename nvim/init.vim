if executable('fish')
    " use fish for embedded terminals
    set shell=fish
    " use bash for else
    let $SHELL = 'bash'
endif

set encoding=utf-8
set nocompatible
filetype off

set laststatus=2
set noshowmode

set directory=$XDG_CACHE_HOME/vim,~/tmp,/var/tmp/,/tmp
set backupdir=$XDG_CACHE_HOME/vim,~/,/tmp

set formatprg=par\ -w80j

set hidden

set foldlevel=20
set foldlevelstart=20

call plug#begin('~/.vim/plugged')
Plug 'neoclide/coc.nvim', {'branch': 'release'}

Plug 'VundleVim/Vundle.vim'
Plug 'junegunn/vim-easy-align'

Plug 'udalov/kotlin-vim'
Plug 'pest-parser/pest.vim'

Plug 'unblevable/quick-scope'
" Plug 'altercation/vim-colors-solarized'
Plug 'lifepillar/vim-solarized8'
Plug 'xiyaowong/nvim-transparent'
Plug 'vim-ruby/vim-ruby'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
" Plugin 'mileszs/ack.vim'
Plug 'rust-lang/rust.vim'
Plug 'nvim-lua/plenary.nvim'
Plug 'saecki/crates.nvim'

Plug 'bronson/vim-trailing-whitespace'
Plug 'Shougo/vimproc.vim'
Plug 'lervag/vimtex'
Plug 'petRUShka/vim-opencl'
Plug 'peterhoeg/vim-qml'
Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'
Plug 'Glench/Vim-Jinja2-Syntax'
Plug 'lumiliet/vim-twig'
Plug 'machakann/vim-sandwich'
Plug 'tomtom/tcomment_vim'
Plug 'martinda/Jenkinsfile-vim-syntax'
" Plug 'rhysd/vim-grammarous'
Plug 'andymass/vim-matchup'
Plug 'cespare/vim-toml'
Plug 'beyondmarc/opengl.vim'
" Plug 'tikhomirov/vim-glsl.git'

" Plug 'cyberkov/openhab-vim.git'

" Git flow
Plug 'airblade/vim-gitgutter'
Plug 'jreybert/vimagit'

Plug 'markonm/traces.vim'
" Plugin 'idanarye/vim-vebugger'
"
Plug 'niklasl/vim-rdf'

Plug 'godlygeek/tabular'
Plug 'plasticboy/vim-markdown'

" source ~/.vim/local-plugins.vim

" Plugin 'tbodt/deoplete-tabnine', { 'do': './install.sh' }
call plug#end()

lua require('crates').setup{ src = { coq = { enabled = true, name = "crates.nvim" } }}

let g:transparent_enabled = v:true

nmap ga <Plug>(EasyAlign)
xmap ga <Plug>(EasyAlign)

filetype plugin indent on
syntax enable

hi clear SpellBad
hi SpellBad cterm=underline ctermfg=red
hi SpellBad gui=undercurl

set tabstop=4
set shiftwidth=4
set expandtab

set cindent
set smartindent
set autoindent

set relativenumber
set undofile

set foldlevel=20
set foldlevelstart=20
set foldcolumn=3

let g:mapleader = ' '

map <leader>t :term<cr>
" map <leader>ltc :GrammarousCheck --lang=en-GB<cr>
" nmap <leader>ltf <Plug>(grammarous-fixit)
" nmap <leader>ltr <Plug>(grammarous-remove-error)
" nmap <leader>ltn <Plug>(grammarous-move-to-next-error)
" nmap <leader>ltp <Plug>(grammarous-move-to-previous-error)

" Limit popup menu height
set pumheight=20

" Generic tab complete
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <c-space> to trigger completion.
if has('nvim')
  inoremap <silent><expr> <c-space> coc#refresh()
else
  inoremap <silent><expr> <c-@> coc#refresh()
endif

" Use <cr> to confirm completion, `<C-g>u` means break undo chain at current
" position. Coc only does snippet and additional edit on confirm.
" <cr> could be remapped by other vim plugin, try `:verbose imap <CR>`.
if exists('*complete_info')
  inoremap <expr> <cr> complete_info()["selected"] != "-1" ? "\<C-y>" : "\<C-g>u\<CR>"
else
  inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
endif

let g:UltiSnipsExpandTrigger="<c-e>"

" FZF
nmap <leader>ff :CocCommand fzf-preview.ProjectFiles<cr>
"nmap <leader>ff :Files<cr>
nmap <leader>fg :CocCommand fzf-preview.GitFiles<cr>
"nmap <leader>fg :GFiles<cr>
"nmap <leader>r :Tags<cr>
"nmap <leader>R :BTags<cr>
nmap <leader>a :CocCommand fzf-preview.ProjectGrep .<cr>
"nmap <leader>a :Find
nmap <leader>b :CocCommand fzf-preview.Buffers<cr>
"nmap <leader>b :Buffers<cr>

"command! -bang -nargs=* Find call fzf#vim#grep('rg --column --line-number --no-heading --fixed-strings --ignore-case --follow --glob "!.git/*" --color "always" '.shellescape(<q-args>).'| tr -d "\017"', 1, <bang>0)

nmap <leader>c :cclose<cr>:pclose<cr>
"nmap <leader>o "zyiw:Files<cr><C-r>z " TODO: how the fuck do I do this?

:set spellfile=~/.vim/spell/en-gb.utf-8.add

set ignorecase
set smartcase

set number
set scrolloff=10

"set concealcursor=vini

augroup GeneralConfig
  au!
  autocmd Filetype xml setlocal ts=2 sts=2 sw=2
  autocmd Filetype eruby setlocal ts=2 sts=2 sw=2
  autocmd Filetype javascript setlocal ts=2 sts=2 sw=2
  autocmd Filetype cpp setlocal ts=4 sts=4 sw=4
  autocmd Filetype rkt setlocal ts=2 sts=2 sw=2
  autocmd Filetype js setlocal ts=2 sts=2 sw=2
  autocmd Filetype m setlocal ts=2 sts=2 sw=2
  autocmd Filetype yml setlocal ts=2 sts=2 sw=2
  autocmd Filetype yaml setlocal ts=2 sts=2 sw=2
  autocmd Filetype ttl setlocal ts=2 sts=2 sw=2
  autocmd Filetype vim setlocal ts=2 sts=2 sw=2

  autocmd BufNewFile,BufRead ~/src/OpenRCT2/* set noexpandtab
  autocmd BufNewFile,BufRead ~/src/MPD/* set noexpandtab
  autocmd BufEnter $HOME/src/TheBounty-Core/* set noexpandtab
  autocmd BufEnter *debian.rules* set noexpandtab
  autocmd BufEnter *Makefile* set noexpandtab

  au BufWritePost .vimrc,_vimrc,vimrc,.gvimrc,_gvimrc,gvimrc so $MYVIMRC | if has('gui_running') | so $MYGVIMRC | endif
augroup END

let g:grammarous#languagetool_cmd = 'languagetool'
let g:sandwich#recipes = deepcopy(g:sandwich#default_recipes)

if executable('rg')
  let g:ackprg = 'rg --vimgrep'
endif

set mouse=a
"set ttymouse=xterm2

let g:GPGPreferArmor=1
let g:GPGDefaultRecipients=["me@rubdos.be"]
let g:copilot_filetypes = {
      \ 'tex': v:false,
      \ 'latex': v:false,
      \ 'vimwiki': v:false,
      \ }
