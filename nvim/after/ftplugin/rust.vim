nmap <leader>gdb :call vebugger#gdb#start('
set foldlevelstart=20
set foldmethod=syntax
let g:rust_fold=2
let g:rustfmt_autosave=1
