nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use K to show documentation in preview window.
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  elseif &filetype ==# 'tex'
    VimtexDocPackage
  elseif (coc#rpc#ready())
    call CocActionAsync('doHover')
  else
    execute '!' . &keywordprg . " " . expand('<cword>')
  endif
endfunction

" Symbol renaming.
nmap <leader>rn <Plug>(coc-rename)

:set tags^=./.git/tags;

xmap <silent> <leader>a  <Plug>(coc-codeaction-selected)
nmap <silent> <F5> <Plug>(coc-codeaction-cursor)
nmap <silent> <F6> <Plug>(coc-codeaction)
" xmap <silent> <F5> <Plug>(coc-codeaction-cursor)
nmap <silent> <leader>qf  <Plug>(coc-fix-current)
nmap <silent> <F2> <Plug>(coc-rename)

" Select references from coc.nvim (only coc extensions)
nmap <leader>cr :CocCommand fzf-preview.CocReferences<cr>

" Select diagnostics from coc.nvim (only coc extensions)
nmap <leader>cD :CocCommand fzf-preview.CocDiagnostics<cr>

" Select current file diagnostics from coc.nvim (only coc extensions)
":CocCommand fzf-preview.CocCurrentDiagnostics<cr>

" Select type definitions from coc.nvim (only coc extensions)
nmap <leader>cd :CocCommand fzf-preview.CocTypeDefinitions<cr>

" Select implementations from coc.nvim (only coc extensions)
nmap <leader>ci :CocCommand fzf-preview.CocImplementations<cr>
