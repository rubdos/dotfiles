set termguicolors
set background=dark

let g:solarized_italics = 1

autocmd vimenter * ++nested colorscheme solarized8
autocmd vimenter * ++nested hi CocHintSign guifg=#586e75
