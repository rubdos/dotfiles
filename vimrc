set encoding=utf-8
set nocompatible
filetype off

set laststatus=2
set noshowmode

set directory=$XDG_CACHE_HOME/vim,~/tmp,/var/tmp/,/tmp
set backupdir=$XDG_CACHE_HOME/vim,~/,/tmp

set formatprg=par\ -w80j

set hidden

set foldlevel=20
set foldlevelstart=20

set runtimepath+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'VundleVim/Vundle.vim'
Plugin 'junegunn/vim-easy-align'

Plugin 'udalov/kotlin-vim'
Plugin 'pest-parser/pest.vim'

Plugin 'unblevable/quick-scope'
" Plugin 'altercation/vim-colors-solarized'
Plugin 'vim-ruby/vim-ruby'
Plugin 'junegunn/fzf.vim'
" Plugin 'mileszs/ack.vim'
Plugin 'rust-lang/rust.vim'
Plugin 'bronson/vim-trailing-whitespace'
Plugin 'Shougo/vimproc.vim'
Plugin 'lervag/vimtex'
Plugin 'petRUShka/vim-opencl'
Plugin 'peterhoeg/vim-qml'
Plugin 'SirVer/ultisnips'
Plugin 'honza/vim-snippets'
Plugin 'Glench/Vim-Jinja2-Syntax'
Plugin 'lumiliet/vim-twig'
Plugin 'machakann/vim-sandwich'
Plugin 'tomtom/tcomment_vim'
Plugin 'martinda/Jenkinsfile-vim-syntax'
Plugin 'rhysd/vim-grammarous'
Plugin 'andymass/vim-matchup'
Plugin 'cespare/vim-toml'
Plugin 'beyondmarc/opengl.vim'
Plugin 'tikhomirov/vim-glsl.git'

Plugin 'cyberkov/openhab-vim.git'

" Git flow
Plugin 'airblade/vim-gitgutter'
Plugin 'jreybert/vimagit'

Plugin 'autozimu/LanguageClient-neovim', {
    \ 'branch': 'next',
    \ 'do': 'bash install.sh',
    \ }

Plugin 'markonm/traces.vim'
" Plugin 'idanarye/vim-vebugger'
"
Plugin 'niklasl/vim-rdf'

Plugin 'godlygeek/tabular'
Plugin 'plasticboy/vim-markdown'

source ~/.vim/local-plugins.vim

" Plugin 'tbodt/deoplete-tabnine', { 'do': './install.sh' }
call vundle#end()
let g:deoplete#enable_at_startup = 1

nmap ga <Plug>(EasyAlign)
xmap ga <Plug>(EasyAlign)

filetype plugin indent on
syntax enable

set background=dark

hi clear SpellBad
hi SpellBad cterm=underline ctermfg=red
hi SpellBad gui=undercurl

"colorscheme solarized

set tabstop=4
set shiftwidth=4
set expandtab

set cindent
set smartindent
set autoindent

set relativenumber
set undofile

set foldlevel=20
set foldlevelstart=20
set foldcolumn=3

let g:mapleader = ' '

map <leader>t :term<cr>
" map <leader>ltc :GrammarousCheck --lang=en-GB<cr>
" nmap <leader>ltf <Plug>(grammarous-fixit)
" nmap <leader>ltr <Plug>(grammarous-remove-error)
" nmap <leader>ltn <Plug>(grammarous-move-to-next-error)
" nmap <leader>ltp <Plug>(grammarous-move-to-previous-error)

" Limit popup menu height
set pumheight=20

" Generic tab complete
inoremap <expr><tab> pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr><s-tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
inoremap <expr><cr> pumvisible() ? "\<C-y>" : "\<cr>"

" LanguageClient
nnoremap <F5> :call LanguageClient_contextMenu()<CR>
" Or map each action separately
nnoremap <silent> K :call LanguageClient#textDocument_hover()<CR>
nnoremap <silent> gd :call LanguageClient#textDocument_definition()<CR>
nnoremap <silent> <F2> :call LanguageClient#textDocument_rename()<CR>
" nnoremap ??? :call LanguageClient#textDocument_formatting()<CR>

let g:UltiSnipsExpandTrigger="<c-e>"

" FZF
nmap <leader>ff :Files<cr>
nmap <leader>fg :GFiles<cr>
nmap <leader>r :Tags<cr>
nmap <leader>R :BTags<cr>
nmap <leader>a :Find<cr>
nmap <leader>b :Buffers<cr>

command! -bang -nargs=* Find call fzf#vim#grep('rg --column --line-number --no-heading --fixed-strings --ignore-case --follow --glob "!.git/*" --color "always" '.shellescape(<q-args>).'| tr -d "\017"', 1, <bang>0)

nmap <leader>c :cclose<cr>:pclose<cr>
nmap <leader>o "zyiw:Files<cr><C-r>z " TODO: how the fuck do I do this?

:set spellfile=~/.vim/spell/en-gb.utf-8.add

set ignorecase
set smartcase

imap jk <Esc>
imap kj <Esc>

set number
set scrolloff=10

"set concealcursor=vini

augroup GeneralConfig
  au!
  autocmd Filetype xml setlocal ts=2 sts=2 sw=2
  autocmd Filetype eruby setlocal ts=2 sts=2 sw=2
  autocmd Filetype javascript setlocal ts=2 sts=2 sw=2
  autocmd Filetype cpp setlocal ts=4 sts=4 sw=4
  autocmd Filetype rkt setlocal ts=2 sts=2 sw=2
  autocmd Filetype js setlocal ts=2 sts=2 sw=2
  autocmd Filetype m setlocal ts=2 sts=2 sw=2
  autocmd Filetype yml setlocal ts=2 sts=2 sw=2
  autocmd Filetype yaml setlocal ts=2 sts=2 sw=2
  autocmd Filetype ttl setlocal ts=2 sts=2 sw=2
  autocmd Filetype vim setlocal ts=2 sts=2 sw=2

  autocmd BufNewFile,BufRead ~/src/OpenRCT2/* set noexpandtab
  autocmd BufNewFile,BufRead ~/src/MPD/* set noexpandtab
  autocmd BufEnter $HOME/src/TheBounty-Core/* set noexpandtab
  autocmd BufEnter *debian.rules* set noexpandtab
  autocmd BufEnter *Makefile* set noexpandtab

  au BufWritePost .vimrc,_vimrc,vimrc,.gvimrc,_gvimrc,gvimrc so $MYVIMRC | if has('gui_running') | so $MYGVIMRC | endif
augroup END

let g:grammarous#languagetool_cmd = 'languagetool'
let g:sandwich#recipes = deepcopy(g:sandwich#default_recipes)

if executable('rg')
  let g:ackprg = 'rg --vimgrep'
endif

set mouse=a
set ttymouse=xterm2
